import React, { useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client';
import { Layout } from 'components/Layout';
import Wrapper from 'components/Wrapper';
import CheckAuth from 'components/CheckAuth';
import Login from 'pages/login';
import Dashboard from 'pages/dashboard';
import Page404 from 'pages/404';

const client = new ApolloClient({
  uri: 'http://localhost:4000',
  cache: new InMemoryCache()
});

function App() {
  const [isAuth, setIsAuth] = useState(false);
  return (
    <BrowserRouter>
      <ApolloProvider client={client}>
        <Switch>
          <Route exact path="/login" component={Login} />
          <Route path="/">
            <Layout>
              <CheckAuth
                setAuth={() => {
                  !isAuth && setIsAuth(true);
                }}
              />
              <Wrapper loading={!isAuth}>
                <Switch>
                  <Route exact path="/" component={Dashboard} />
                  <Route component={Page404} />
                </Switch>
              </Wrapper>
            </Layout>
          </Route>
        </Switch>
      </ApolloProvider>
    </BrowserRouter>
  );
}

export default App;
