import React, { lazy, Suspense, useCallback } from 'react';
import Wrapper from 'components/Wrapper';
import { Container } from 'components/Layout';
const Dashboard = lazy(() => import('containers/Dashboard'));

const DashboardPage = () => {
  const onLogout = useCallback(() => {
    // force update
    window.location.replace(`/`);
  }, []);

  return (
    <Suspense fallback={<Wrapper />}>
      <Container>
        <Dashboard onLogout={onLogout} />
      </Container>
    </Suspense>
  );
};

export default DashboardPage;
