import React, { PropsWithChildren } from 'react';
import { Header } from 'components/Header';

export const Layout = ({ children }: PropsWithChildren<Record<string, unknown>>) => {
  return (
    <div>
      {/* root class */}
      <Header />
      <div>
        {/* content class */}
        {children}
      </div>
    </div>
  );
};

export const Container = ({ children }: PropsWithChildren<Record<string, unknown>>) => {
  return (
    <div>
      {/* container class */}
      {children}
    </div>
  );
};
