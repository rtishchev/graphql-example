import React, { useEffect } from 'react';
import { goToLogin } from 'utils/goToLogin';
import cookie from 'cookie';

const CheckAuth = ({ setAuth }: { setAuth: () => void }) => {
  useEffect(() => {
    const cookies = cookie.parse(document.cookie);

    if (!cookies.isAuth) {
      goToLogin();
    } else {
      setAuth();
    }
  }, [setAuth]);

  return <></>;
};

export default CheckAuth;
