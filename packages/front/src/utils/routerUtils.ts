import { useHistory } from 'react-router';
import { parse, stringify } from 'query-string';
import { useMemo } from 'react';

export type Params<T = string | number | boolean> = Record<string, T | T[] | null | undefined>;

export interface Query {
  params: Params;
  push: (params: Params) => void;
  clear: () => void;
}

export const useQuery = (): Query => {
  const history = useHistory();

  const params = useMemo<Params>(() => {
    return parse(history.location.search, { parseNumbers: true, parseBooleans: true });
  }, [history.location]);

  return {
    params: params,
    push: (replace: Params) => {
      history.push({ search: stringify({ ...params, ...replace }) });
    },
    clear: () => {
      history.push({ search: undefined });
    },
  };
};
