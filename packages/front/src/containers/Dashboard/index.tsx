import React from 'react';
import { gql, useQuery } from '@apollo/client';
import LogOutButton from 'components/LogoutButton';
import { ReactComponent as Icon } from 'icons/antispy.svg';
import s from './Dashboard.module.scss';
import Wrapper from 'components/Wrapper/Wrapper';
import { GetMoviesQuery } from 'types/Queries';

interface Props {
  onLogout?: () => void;
}
const DirectorFieldsFragment = gql`
    fragment DirectorFields on Director {
        name
    }
`;
const BaseFieldsFragment = gql`  
  fragment MovieFields on Movie {
    id
      name
  }
`;

const GET_MOVIES = gql`
  query GetMovies($ids: [ID!]!) {
    inner {
      movies(movieIds: $ids) {
        name
        director {
          id
          name
        }
      },
      entities(limit: 4) {
          __typename
          ...DirectorFields
          ...MovieFields
      }
    }
  }
  ${BaseFieldsFragment}
  ${DirectorFieldsFragment}
`;

const DashboardPage = ({ onLogout }: Props) => {
  const { loading, error, data } = useQuery<GetMoviesQuery>(GET_MOVIES, {
    variables: { ids: [1, 2] },
  });
  return (
    <div className={s.root}>
      <h1>Dashboard</h1>
      <Icon />
      <LogOutButton onSuccess={onLogout} />
      <Wrapper loading={loading} error={!!error} errorMessage={error?.message}>
        <ul>
        {data?.inner?.entities?.map(((e,k) => (
          <li key={k}>{e.__typename} {e.name}</li>
        )))}
        </ul>
      </Wrapper>
    </div>
  );
};

export default DashboardPage;
