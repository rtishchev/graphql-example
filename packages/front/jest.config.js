module.exports = {
    setupFiles: ['<rootDir>/enzyme.config.js'],
    roots: ['<rootDir>/src'],
    transform: {
        '^.+\\.tsx?$': 'ts-jest',
        ".+\\.(css|styl|less|sass|scss|svg|png|jpg|ttf|woff|woff2)$": "jest-transform-stub",
    },
    testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$',
    moduleDirectories: ['node_modules', 'src'],
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
    snapshotSerializers: ["enzyme-to-json/serializer"],
    globals: {
        'ts-jest': {
            tsConfig: '<rootDir>/tsconfig.json'
        }
    },
    moduleNameMapper: {
        "\\.svg": "<rootDir>/src/__mocks__/svgMock.ts",
        "\\.(scss|sass|css)$": "<rootDir>/src/__mocks__/cssMock.ts"
    },
    coverageThreshold: {
        global: {
            branches: 0,
            functions: 0,
            lines: 50,
            statements: 50
        }
    }
};
