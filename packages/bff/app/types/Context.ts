import { GraphQLResolveInfo } from 'graphql';
import { Request } from 'express';

export type CustomMethod = (info?: GraphQLResolveInfo) => boolean;

export interface Context {
  req: Request,
  customMethod: CustomMethod
}