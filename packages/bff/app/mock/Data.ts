export interface IDirector {
  id: number;
  name: string;
  movieIds: number[];
}
export interface IMovie {
  id: number;
  name: string;
  genre: string;
  directorId: number;
}
export const Movies: IMovie[] = [
  {id: 1, name: 'first', genre: 'horror', directorId: 1},
  {id: 2, name: 'second', genre: 'action', directorId: 2},
  {id: 3, name: 'third', genre: 'drama', directorId: 2},
];

export const Directors: IDirector[] = [
  {id: 1, name: 'vasya', movieIds: [1]},
  {id: 2, name: 'fedya', movieIds: [2, 3]},
];