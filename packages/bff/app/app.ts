import { ApolloServer, IResolvers } from 'apollo-server';
import { mainSchema } from 'common/schemas';
// import { getPathFromInfo } from 'helpers/getPathFromInfo';
import { Context, CustomMethod } from 'types/Context';
import resolvers from 'resolvers';

const server = new ApolloServer({ typeDefs: mainSchema, resolvers: resolvers as IResolvers<any, Context>, context: async ({req}) => {
  const customMethod: CustomMethod = (info) => {
    // const path = getPathFromInfo(info);
    // console.log('get access', path);
    return true;
  };
  return {req, customMethod};
}});

server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
